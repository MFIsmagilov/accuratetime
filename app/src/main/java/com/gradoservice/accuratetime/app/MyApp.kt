package com.gradoservice.accuratetime.app

import android.app.Application
import android.os.StrictMode
import android.widget.Toast
import com.gradoservice.accuratetime.time.AccurateTime
import com.squareup.leakcanary.LeakCanary
import com.squareup.leakcanary.RefWatcher


class MyApp : Application() {

    private lateinit var refWatcher: RefWatcher

    init {
        instance = this
    }

    private lateinit var accurateTime: AccurateTime

    override fun onCreate() {
        super.onCreate()
        setupLeakCanary()
//        accurateTime = AccurateTime1.create()
//                .withContext(applicationContext)
//                .withNtpServer("time.google.com")
//                .build()
//        AccurateTime.create()
//                .withContext(this)
//                .withCache(true)
//                .syncTime()
    }


    fun mustDie(obj: Any) {
        refWatcher.watch(obj)
    }

    private fun setupLeakCanary() {
        if (LeakCanary.isInAnalyzerProcess(this)) {
            Toast.makeText(this, "isInAnalyzerProcess", Toast.LENGTH_LONG).show()
            return
        }
        enabledStrictMode()
        refWatcher = LeakCanary.install(this)
    }

    private fun enabledStrictMode() {
        StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .penaltyDeath()
                .build())
    }

    companion object {
        private lateinit var instance: MyApp

        @JvmStatic
        fun get() = instance

    }
}