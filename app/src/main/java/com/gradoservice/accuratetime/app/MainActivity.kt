package com.gradoservice.accuratetime.app

import android.os.Bundle
import android.os.SystemClock
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.gradoservice.accuratetime.time.AccurateTime
import com.gradoservice.accuratetime.time.timeproviders.GeoLocationTime
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.plugins.RxJavaPlugins
import kotlinx.android.synthetic.main.new_activity_main.*
import org.apache.commons.net.ntp.NTPUDPClient
import java.net.InetAddress
import java.text.SimpleDateFormat
import java.util.*
import kotlin.concurrent.thread


class MainActivity : AppCompatActivity() {


    private lateinit var geoTime: GeoLocationTime

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.new_activity_main)
        AccurateTime.create()
                .withContext(this)
                .withCache(true)
                .syncTime()


        RxJavaPlugins.setErrorHandler {
            it.printStackTrace()
        }
        gmtTime.text = ""

        AccurateTime.isSyncComplete()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    gmtTime.text = gmtTime.text.toString() + " " + it.toString()
                }

        button2.setOnClickListener {
//            utcTime.text = AccurateTime.now().formatUtcTime()
            gmtTime.text = AccurateTime.now().formatUtcTime()
            gmtTime.text = gmtTime.text.toString() + "\n" + AccurateTime.now().formatGmtTime(3)
            gmtTime.text = gmtTime.text.toString() + "\n" + AccurateTime.now().formatGmtTime(TimeZone.getDefault().rawOffset)
        }

        button3.setOnClickListener {
            AccurateTime.reSyncTime()
        }
    }

    fun getTimeFromNtpServer() {
        thread(start = true) {
            val TIME_SERVER = "time.google.com"
            val timeClient = NTPUDPClient()
            val inetAddress = InetAddress.getByName(TIME_SERVER)
            val timeInfo = timeClient.getTime(inetAddress)
            //long returnTime = timeInfo.getReturnTime();   //local device time
            val returnTime = timeInfo.message.transmitTimeStamp.time   //server time
            val ellapsed = SystemClock.elapsedRealtime()
            val delta = returnTime - ellapsed
            log("rettune: $returnTime")
            log("Ellapse: $ellapsed")
            log("Delta  : $delta")
            log("Delt+El: ${delta + ellapsed}")

            val time = Date(returnTime)
            formatUtcTime(time)
            Log.e("getCurrentNetworkTime", "Time from $TIME_SERVER: $time")

            Log.e("Local time", "Local time")
            Log.e("Local time", "Current time: " + Date(System.currentTimeMillis()))
            Log.e("Local time", "Time info: " + Date(timeInfo.returnTime))
            Log.e("Local time", "GetOriginateTimeStamp: " + Date(timeInfo.message.originateTimeStamp.time))

            Log.e("NTP time", "Time from $TIME_SERVER: $time")

            Log.e("Local time", "Time info: " + Date(timeInfo.message.receiveTimeStamp.time))
            Log.e("Local time", "GetOriginateTimeStamp: " + Date(timeInfo.message.transmitTimeStamp.time))
        }
    }

    private val dispoables: CompositeDisposable = CompositeDisposable()

    fun formatUtcTime(date: Date) {
        val df = SimpleDateFormat("dd.MM.yyyy hh:mm:ss")
        df.timeZone = TimeZone.getTimeZone("UTC")
        val strDate = df.format(date)
        log(strDate)
    }

    fun log(message: Any) {
        Log.d("MA", message.toString())
    }

    override fun onPause() {
        super.onPause()
        dispoables.clear()
    }

    override fun onDestroy() {
        super.onDestroy()
        println("onDestroy")
        AccurateTime.dispose(this)
        MyApp.get().mustDie(this)
    }
}
