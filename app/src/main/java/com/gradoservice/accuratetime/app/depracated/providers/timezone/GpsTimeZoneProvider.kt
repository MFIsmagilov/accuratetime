package com.gradoservice.accuratetime.app.depracated.providers.timezone

import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.util.Log
import io.reactivex.subjects.BehaviorSubject
import java.util.*


class GpsTimeZoneProvider(
        context: Context,
        minTime: Long = 2000,
        minDistance: Float = 10.0f) : TimeZoneProvider, LocationListener {


    private var locationSubject: BehaviorSubject<Int> = BehaviorSubject.create()
    private var locationManager: LocationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager


    private var timeZone: TimeZone

    init {
        if (Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            throw Exception("Нет разрешения на Геолокацию!")
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTime, minDistance, this)
        timeZone = TimeZone.getTimeZone("UTC")
    }

    override fun getOffset(): BehaviorSubject<Int> = locationSubject

    override fun onLocationChanged(location: Location?) {
        Log.d("GpsTimeZoneProvider", Date(location?.time ?: 0).toString())
        location?.let {
            locationSubject.onNext(timeZone.getOffset(it.time))
        }
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
    }

    override fun onProviderEnabled(provider: String?) {
    }

    override fun onProviderDisabled(provider: String?) {
    }

}