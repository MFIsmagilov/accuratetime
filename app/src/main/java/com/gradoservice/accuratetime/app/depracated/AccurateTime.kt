package com.gradoservice.accuratetime.app.depracated

import android.content.Context
import android.net.ConnectivityManager
import com.gradoservice.accuratetime.time.timeproviders.GeoLocationTime
import com.gradoservice.accuratetime.time.timeproviders.NtpServerTime
import com.gradoservice.accuratetime.time.model.Time
import io.reactivex.Single
import java.lang.ref.WeakReference


class AccurateTime private constructor() {

    private var delta = 0L


    private var isNeedSharedPreferencesCache = false
    private var ntpServerHost = "time.google.com"
    private lateinit var refContext: WeakReference<Context>
    private lateinit var ntpObservable: NtpServerTime// = NtpServerTime()
    private lateinit var geoObservable: GeoLocationTime// = GeoLocationTime(context)

    companion object {
        private lateinit var instance: AccurateTime

        @JvmStatic
        fun create(): AccurateTime {
            instance = AccurateTime()
            return instance
        }
    }

    fun withContext(context: Context): AccurateTime {
        refContext = WeakReference(context)
        return this
    }

    fun withSharedPreferenceCache(): AccurateTime {
        refContext.get()?.let {
            isNeedSharedPreferencesCache = true
            return this
        }
        throw NullContextException()
    }

    fun withNtpServerHost(host: String): AccurateTime {
        ntpServerHost = host
        return this
    }

    fun getUtcTime(): Single<Time> {
        return (if (isNetworkConnected()) {
            initNtpObservable()
            ntpObservable
                    .getUtcTime()
        } else {
            initGeoObservable()
            geoObservable
                    .getUtcTime()
        })
                .map {
                    if (isNeedSharedPreferencesCache) {
                    }
                    it
                }
    }

    private fun initNtpObservable() {
        ntpObservable = NtpServerTime(ntpServerHost)
    }

    private fun initGeoObservable() {
        refContext.get()?.let {
            geoObservable = GeoLocationTime(it)
        }
        throw NullContextException()
    }

    private fun isNetworkConnected(): Boolean {
        refContext.get()?.let {
            val connectivityManager = it.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo = connectivityManager.activeNetworkInfo
            return networkInfo != null && networkInfo.isConnected
        }
        return false
    }
}

class NullContextException : Exception("Context is null!")

