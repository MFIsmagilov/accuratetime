package com.gradoservice.accuratetime.app.depracated.providers.timezone

import io.reactivex.subjects.BehaviorSubject

interface TimeZoneProvider {

    fun getOffset(): BehaviorSubject<Int>

}