package com.gradoservice.accuratetime.app.depracated

import android.content.Context
import android.net.ConnectivityManager
import android.os.SystemClock
import android.util.Log
import com.gradoservice.accuratetime.time.model.Time
import com.gradoservice.accuratetime.time.model.TimeFrom
import com.gradoservice.accuratetime.time.timeproviders.DiskCacheTime
import com.gradoservice.accuratetime.time.timeproviders.GeoLocationTime
import com.gradoservice.accuratetime.time.timeproviders.NtpServerTime
import com.gradoservice.accuratetime.time.timeproviders.VariableCacheTime
import io.reactivex.Single
import java.lang.ref.WeakReference


class AccurateTime1 private constructor() {

    private val TAG = "AccurateTime"

    private lateinit var refContext: WeakReference<Context>
    private var isNeedCache: Boolean = false // сохранение в SharedPrefernce delta
    private var ntpHost: String = "time.google.com"
    private lateinit var ntpObservable: NtpServerTime
    private lateinit var geoObservable: GeoLocationTime
    private lateinit var cacheObservable: DiskCacheTime
    private var variableCacheTime: VariableCacheTime? = null

    companion object {

        @JvmStatic
        fun create(): Builder {
            return AccurateTime1()
                    .Builder()
        }
    }


    inner class Builder constructor() {
        fun withContext(context: Context): Builder {
            this@AccurateTime1.refContext = WeakReference(context)
            return this
        }

        fun withNtpServer(host: String): Builder {
            this@AccurateTime1.ntpHost = host
            return this
        }

        fun withCache(isNeedCache: Boolean): Builder {
            this@AccurateTime1.isNeedCache = isNeedCache
            return this
        }

        fun build(): AccurateTime1 {
            return this@AccurateTime1
        }
    }


    private fun log(message: Any) {
        Log.d(TAG, message.toString())
    }

    fun now(): Time {

        variableCacheTime?.let {
            return it.getUtcTime()
        }

        //если в локальном кэше нет то смотрим в prefernce
        cacheObservable.getTime()?.let {
            return it
        }

        //иначе мы либо не успели получить точное время из интернета и gps
        //либо что-то пошло не так... но что?
        return Time(System.currentTimeMillis(), TimeFrom.SYSTEM)
    }

    fun getUtcTime(): Single<Time> {

        return if (variableCacheTime == null || !cacheObservable.isContaintDelta()) {
            getUtcTimeObservable()
                    .map {
                        log(it.formatUtcTime())
                        variableCacheTime = VariableCacheTime(SystemClock.elapsedRealtime() - it.time, it.from)
                        if (isNeedCache) {
                            cacheObservable.saveDeltaTime(it)
                        }
                        it
                    }
        } else {
            cacheObservable.getUtcTime()
                    .map {
                        log(it.formatUtcTime())
                        variableCacheTime = VariableCacheTime(SystemClock.elapsedRealtime() - it.time, it.from)
                        it
                    }
        }
    }

    private fun getUtcTimeObservable(): Single<Time> {
        refContext.get()?.let {
            return if (isNetworkConnected()) {
                ntpObservable = NtpServerTime(ntpHost)
                ntpObservable.getUtcTime()
            } else {
                geoObservable = GeoLocationTime(it)
                geoObservable.getUtcTime()
            }
        }
        return Single.create({ emitter -> emitter.onError(NullContextException()) })
    }

    private fun isNetworkConnected(): Boolean {
        refContext.get()?.let {
            val connectivityManager = it.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo = connectivityManager.activeNetworkInfo
            return networkInfo != null && networkInfo.isConnected
        }
        return false
    }
}