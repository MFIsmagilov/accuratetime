AccurateTime
========
[![Repo Geo4.pro](https://img.shields.io/badge/Repo%20Geo4.pro-0.4-brightgreen.svg)](http://repo.geo4.pro/artifactory/android/com/gradoservice/android/accurate-time/)

Получение точного времени с NTP сервера (по умолчанию time.google.com) или GPS.

app //пример использования
accurate-time-lib //библиотека


Подключение к проекту
--------


Gradle:
```groovy
    implementation ("com.gradoservice.android:accurate-time:x.y@aar"){
        transitive = true
    }
```

Пример
========

В onCreate приложения вызываем

```java
AccurateTime
        .withContext(applicationContext)
        .withNtpHost("time.google.com")
        .withCache(true)
        .build()
        .syncTime()
```

В дальнейшем

```java
AccurateTime.now()
```

Описание
========

После этого библиотека будет пытаться получить точное время с NTP сервера, если
он будет не доступен или у пользователя нет интернета то пробуем получить точное
время с геоточки.
Если в то время когда происходит ожидание геоточки и появляется интернет то будет
попытка получить время с NTP.

Достаточно получить точное время только один раз (до перезагрузки девайса)
в sharedPreference будет записано следующее:
delta = accuratetime - elapsedRealTime //разница между точным временем и временем работы девайса

timeFrom //  откуда мы получили время Network или Location

elapsedRealTime // время работы девайса после последней загрузки

и время будет получаться по формуле delta + elapsedRealTime.

Внимание
========

Если .syncTime() не успел получить точное время то AccurateTime.now() будет
возращать системное время.
Чтобы узнать было получено точное время используется следующее:
```Kotlin
        AccurateTime.isSyncComplete()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    //true если успешн
                    log("TimeActivity", "isSyncComplete = $it")
                }
```