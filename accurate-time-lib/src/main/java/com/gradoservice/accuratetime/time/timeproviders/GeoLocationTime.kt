package com.gradoservice.accuratetime.time.timeproviders

import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.support.v4.content.ContextCompat
import com.gradoservice.accuratetime.time.log
import com.gradoservice.accuratetime.time.model.Time
import com.gradoservice.accuratetime.time.model.TimeFrom
import io.reactivex.Single
import io.reactivex.SingleEmitter
import java.lang.ref.WeakReference

/**
 * Отдает время один раз полученное с геолокация в виде Observable<Long>
 * Если время нужно получить нескольким подписчикам используй оператор .replay()
 */
class GeoLocationTime(context: Context,
                      val minTime: Long = 0,//2000,10.0f
                      val minDistance: Float = 0.0f) : TimeProvider {

    private val TAG = "GeoLocationTime"

    private val refContext: WeakReference<Context> = WeakReference(context)

    override fun getUtcTime(): Single<Time> {

        return Single.create({ emitter ->
            run {
                refContext.get()?.let {
                    log(TAG, "Начинаю работать")
                    var locationManager = it.getSystemService(Context.LOCATION_SERVICE) as LocationManager?
                    if (Build.VERSION.SDK_INT >= 23 &&
                            ContextCompat.checkSelfPermission(it, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                            ContextCompat.checkSelfPermission(it, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        if (!emitter.isDisposed) {
                            //fixme
                            emitter.onError(Exception("Нет разрешения на Геолокацию!"))
                        }
                    }

                    val locationListenerGpsProvider = createLocationListener(emitter)


                    locationManager?.let { lm ->
                        run {
                            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTime, minDistance, locationListenerGpsProvider, Looper.getMainLooper())
                        }
                    }

                    emitter.setCancellable {
                        log(TAG, "setCancellable")
                        locationManager?.removeUpdates(locationListenerGpsProvider)
                        locationManager = null
                    }
                } ?: if (!emitter.isDisposed) {
                    emitter.onError(Exception("Context is null"))
                }
            }
        })
    }

    private fun createLocationListener(emitter: SingleEmitter<Time>): LocationListener {
        return object : LocationListener {
            override fun onLocationChanged(location: Location?) {
                //todo: проверка на фиктовное местоположение
                location?.let {
                    log(TAG, "onLocationChanged = ${it.time} from ${it.provider} isMock = ${isMock(it)}")

                    if (isMock(it)) return

                    if (!emitter.isDisposed) {
                        emitter.onSuccess(Time(it.time, TimeFrom.LOCATION))
                    }
                }
            }

            override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
            }

            override fun onProviderEnabled(provider: String?) {
            }

            override fun onProviderDisabled(provider: String?) {
            }
        }
    }

    private fun isMock(location: Location): Boolean {
        refContext.get()?.let {
            return if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1) {
                Settings.Secure.getString(it.contentResolver, Settings.Secure.ALLOW_MOCK_LOCATION) != "0"
            } else {
                location.isFromMockProvider
            }
        }
        return true
    }
}