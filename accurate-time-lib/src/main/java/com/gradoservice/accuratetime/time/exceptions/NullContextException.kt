package com.gradoservice.accuratetime.time.exceptions

class NullContextException : Exception("Context is null!")
