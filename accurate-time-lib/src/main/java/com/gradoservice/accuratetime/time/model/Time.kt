package com.gradoservice.accuratetime.time.model

import android.os.SystemClock
import java.text.SimpleDateFormat
import java.util.*


class Time(val time: Long, val from: TimeFrom) {
    fun formatUtcTime(pattern: String = "dd.MM.yyyy HH:mm:ss"): String {
        val df = SimpleDateFormat(pattern)
        df.timeZone = TimeZone.getTimeZone("UTC")
        return df.format(Date(time)) + " ${from.nameFrom}"
    }

    fun formatGmtTime(timezone: Int, pattern: String = "dd.MM.yyyy HH:mm:ss z"): String {
        val df = SimpleDateFormat(pattern)
        df.timeZone = TimeZone.getTimeZone("GMT+$timezone")
        return df.format(Date(time)) + " ${from.nameFrom}"
    }

    fun getDelta(): Long {
        return time - SystemClock.elapsedRealtime()
    }

    fun getDeltaBy(time: Long): Long {
        return this.time - time
    }

    companion object {
        fun getTimeWithDelta(delta: Long): Long {
            return delta + SystemClock.elapsedRealtime()
        }
    }
}


enum class TimeFrom(val nameFrom: String) {
    LOCATION("location"),
    NETWORK("network"),
    SYSTEM("system");

    companion object {
        fun from(value: Int) = values().find { it.ordinal == value }
    }
}