package com.gradoservice.accuratetime.time.timeproviders

import com.gradoservice.accuratetime.time.model.Time
import com.gradoservice.accuratetime.time.model.TimeFrom

class VariableCacheTime(val delta: Long, val from: TimeFrom) {
    fun getUtcTime(): Time {
        return Time(Time.getTimeWithDelta(delta), from)
    }
}