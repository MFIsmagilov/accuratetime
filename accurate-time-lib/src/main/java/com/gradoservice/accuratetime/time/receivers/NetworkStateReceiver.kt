package com.gradoservice.accuratetime.time.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import com.gradoservice.accuratetime.time.AccurateTime
import com.gradoservice.accuratetime.time.log


//@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
//class ConnectionStateMonitor : ConnectivityManager.NetworkCallback() {
//    private var networkRequest: NetworkRequest = NetworkRequest.Builder().addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR).addTransportType(NetworkCapabilities.TRANSPORT_WIFI).build()
//
//    fun enable(context: Context) {
//        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
//        connectivityManager.registerNetworkCallback(networkRequest, this)
//    }
//    override fun onAvailable(network: Network) {
//        network.
//    }
//}

class NetworkChangeStateReceiver : BroadcastReceiver() {

    private val TAG = "NetworkChangeStateReceiver"

    override fun onReceive(context: Context?, intent: Intent?) {

        log(TAG, isInitialStickyBroadcast.toString())
        if (!isInitialStickyBroadcast) {

            context?.let {

                val status = NetworkUtil.getConnectivityStatusString(it)
                log(TAG, "onReceive ${status} ${intent?.action}")
                if ("android.net.conn.CONNECTIVITY_CHANGE" == intent?.action) {
                    if (status != NetworkUtil.NETWORK_STATUS_NOT_CONNECTED) {
                        log(TAG, "onReceive start reSync = $status")
                        AccurateTime.reSyncTime()
                    }
                }
            }
        }
    }
}

object NetworkUtil {
    private var TYPE_WIFI = 1
    private var TYPE_MOBILE = 2
    private var TYPE_NOT_CONNECTED = 0
    const val NETWORK_STATUS_NOT_CONNECTED = 0
    const val NETWORK_STAUS_WIFI = 1
    const val NETWORK_STATUS_MOBILE = 2

    private fun getConnectivityStatus(context: Context): Int {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val activeNetwork = cm.activeNetworkInfo
        if (null != activeNetwork) {
            if (activeNetwork.type == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI

            if (activeNetwork.type == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE
        }
        return TYPE_NOT_CONNECTED
    }

    fun getConnectivityStatusString(context: Context): Int {
        val conn = getConnectivityStatus(context)
        var status = 0
        if (conn == TYPE_WIFI) {
            status = NETWORK_STAUS_WIFI
        } else if (conn == TYPE_MOBILE) {
            status = NETWORK_STATUS_MOBILE
        } else if (conn == TYPE_NOT_CONNECTED) {
            status = NETWORK_STATUS_NOT_CONNECTED
        }
        return status
    }
}