package com.gradoservice.accuratetime.time.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.gradoservice.accuratetime.time.log
import com.gradoservice.accuratetime.time.timeproviders.DiskCacheTime


class DeviceBootReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        log(TAG, "onReceive ${intent.action}")
        context.getSharedPreferences("lalala", 0).edit().putLong("work", 1).apply()
        log(TAG, "clearing disk cache as we've detected a boot")
        DiskCacheTime.clear(context)
    }

    companion object {
        private val TAG = DeviceBootReceiver::class.java.simpleName
    }
}
