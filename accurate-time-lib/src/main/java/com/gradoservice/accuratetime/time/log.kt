package com.gradoservice.accuratetime.time

import android.util.Log

fun log(tag: String, message: Any) {
    Log.d("Lib: $tag", message.toString())
}