package com.gradoservice.accuratetime.time.timeproviders

import android.content.Context
import android.os.SystemClock
import com.gradoservice.accuratetime.time.model.Time
import com.gradoservice.accuratetime.time.model.TimeFrom
import io.reactivex.Single
import java.lang.ref.WeakReference

class DiskCacheTime(context: Context) : TimeProvider {


    private val context = WeakReference<Context>(context)

    companion object {
        private const val PREFERENCE_NAME = "com.gradoservice.accuratetime"
        private const val TIME_DELTA = "TIME_DELTA"
        private const val TIME_FROM = "TIME_FROM"
        private const val TIME_DEVICE_ELAPSED_TIME = "TIME_DEVICE_ELAPSED_TIME"

        fun clear(context: Context): Boolean {
            return context.getSharedPreferences(PREFERENCE_NAME, 0).edit().clear().commit()
        }
    }

    override fun getUtcTime(): Single<Time> {
        return Single.create({ emitter ->
            run {
                context.get()?.let {
                    val pref = it.getSharedPreferences(PREFERENCE_NAME, 0)
                    val delta = pref.getLong(TIME_DELTA, Long.MIN_VALUE)
                    val from = pref.getInt(TIME_FROM, -1)
                    val timeFrom = TimeFrom.from(from)
                    if (timeFrom != null && delta != Long.MIN_VALUE) {
                        emitter.onSuccess(Time(Time.getTimeWithDelta(delta), timeFrom))
                    } else {
                        emitter.onError(Exception("В кэше нет времени"))
                    }
                }
                emitter.onError(Exception("Context is null"))
            }
        })
    }

    fun saveDeltaTime(time: Time): Boolean {
        context.get()?.let {
            val pref = it.getSharedPreferences(DiskCacheTime.PREFERENCE_NAME, 0).edit()

            val elapsedTime = SystemClock.elapsedRealtime()
            val delta = time.getDeltaBy(elapsedTime)

            pref.putInt(DiskCacheTime.TIME_FROM, time.from.ordinal)
            pref.putLong(DiskCacheTime.TIME_DELTA, delta)
            pref.putLong(DiskCacheTime.TIME_DEVICE_ELAPSED_TIME, elapsedTime)
            return pref.commit()
        }
        return false
    }

    fun isContaintDelta(): Boolean {
        context.get()?.let {
            val pref = it.getSharedPreferences(PREFERENCE_NAME, 0)
            val delta = pref.getLong(TIME_DELTA, Long.MIN_VALUE)
            val from = pref.getInt(TIME_FROM, -1)
            return delta != Long.MIN_VALUE && from != -1
        }
        return false
    }

    fun getTime(): Time? {
        context.get()?.let {
            val pref = it.getSharedPreferences(PREFERENCE_NAME, 0)
            val deviceElapsedTime = pref.getLong(TIME_DEVICE_ELAPSED_TIME, 0)
            if(SystemClock.elapsedRealtime() < deviceElapsedTime){ // значит была жесткая перезагрузка устройства
                return null
            }
            val delta = pref.getLong(TIME_DELTA, Long.MIN_VALUE)
            val from = pref.getInt(TIME_FROM, -1)
            val timeFrom = TimeFrom.from(from)
            if (timeFrom != null && delta != Long.MIN_VALUE) {
                return Time(Time.getTimeWithDelta(delta), timeFrom)
            }
        }
        return null
    }
}