package com.gradoservice.accuratetime.time.timeproviders

import com.gradoservice.accuratetime.time.log
import com.gradoservice.accuratetime.time.model.Time
import com.gradoservice.accuratetime.time.model.TimeFrom
import io.reactivex.Single
import org.apache.commons.net.ntp.NTPUDPClient
import java.net.InetAddress


class NtpServerTime(private val ntpServer: String) : TimeProvider {

    private val TAG = "NtpServerTime"

    /**
     * Получение UTC время, c ntp сервера
     */
    override fun getUtcTime(): Single<Time> {
        return getMilliseconds()
    }


    private fun getMilliseconds(): Single<Time> {
        return Single.fromCallable {
            log(TAG, "start")
            val timeClient = NTPUDPClient()
            timeClient.defaultTimeout = 2000
            val inetAddress = InetAddress.getByName(ntpServer)
            val timeInfo = timeClient.getTime(inetAddress)
            log(TAG, "end = ${timeInfo.message.transmitTimeStamp.time}")
            Time(timeInfo.message.transmitTimeStamp.time, TimeFrom.NETWORK)
        }.onErrorResumeNext { throwable ->
            run {
                log(TAG, "error = ${throwable.message}")
                Single.error(NtpException(throwable))
            }
        }
    }
}

class NtpException(message: Throwable?) : Exception(message)