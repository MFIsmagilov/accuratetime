package com.gradoservice.accuratetime.time

import android.content.Context
import android.content.IntentFilter
import android.net.ConnectivityManager
import com.gradoservice.accuratetime.time.exceptions.NullContextException
import com.gradoservice.accuratetime.time.model.Time
import com.gradoservice.accuratetime.time.model.TimeFrom
import com.gradoservice.accuratetime.time.receivers.DeviceBootReceiver
import com.gradoservice.accuratetime.time.receivers.NetworkChangeStateReceiver
import com.gradoservice.accuratetime.time.timeproviders.*
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.lang.ref.WeakReference

class AccurateTime private constructor() {

    companion object {
        private val TAG = "AccurateTime"
        private var instance: AccurateTime? = null
        @JvmStatic
        fun create(): AccurateTime {
            instance = AccurateTime()
            instance?.let {
                return@create it
            } ?: throw Exception("AccurateTime is not init")
        }

        fun now(): Time {
            instance?.let {
                return it.now()
            }
            throw Exception("AccurateTime is not init")
        }

        fun isSyncComplete(): Observable<Boolean> {
            instance?.let {
                return it.isSyncCompleteSubject
            }
            throw Exception("AccurateTime is not init")
        }

        fun reSyncTime() {
            instance?.let {
                it.reSyncTime()
                return
            }
            log(TAG, "Во время reSync небыл создан instance") //по идее этого никогда не должно быть
        }

        fun dispose(context: Context) {
            instance?.let {
                context.unregisterReceiver(it.networkChangeStaterReceiver)
                context.unregisterReceiver(it.bootCompletedBroadcastReceiver)
            }
        }

    }


    private var syncTimeWithError = false
    private var isNeedCache: Boolean = false // сохранение в SharedPrefernce delta
    private var ntpHost: String = "time.google.com"
    private lateinit var refContext: WeakReference<Context>
    private lateinit var ntpObservable: NtpServerTime
    private lateinit var geoObservable: GeoLocationTime
    private var cacheObservable: DiskCacheTime? = null
    private var variableCacheTime: VariableCacheTime? = null
    private var disposable: Disposable? = null


    private var networkChangeStaterReceiver: NetworkChangeStateReceiver? = null
    private var bootCompletedBroadcastReceiver: DeviceBootReceiver? = null

    private var isSyncCompleteSubject = BehaviorSubject.create<Boolean>()

    fun withContext(context: Context): AccurateTime {
        refContext = WeakReference(context)

        val intentFilters = IntentFilter()
//        intentFilters.addAction("android.net.conn.CONNECTIVITY_CHANGE")
        intentFilters.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
//        intentFilters.addAction("android.net.wifi.WIFI_STATE_CHANGED")
        networkChangeStaterReceiver = NetworkChangeStateReceiver()
        context.registerReceiver(networkChangeStaterReceiver, intentFilters)


        val intentFiltersForBootCompleted = IntentFilter()
        intentFiltersForBootCompleted.addAction("android.intent.action.BOOT_COMPLETED")
        intentFiltersForBootCompleted.addAction("android.intent.action.QUICKBOOT_POWERON")
        intentFiltersForBootCompleted.addAction("android.intent.category.DEFAULT")
        intentFiltersForBootCompleted.addAction("android.intent.action.REBOOT")
        intentFiltersForBootCompleted.addAction("com.htc.intent.action.QUICKBOOT_POWEROFF")
        intentFiltersForBootCompleted.addAction("android.intent.action.ACTION_SHUTDOWN") //кажется нужно оставить только этоту
        bootCompletedBroadcastReceiver = DeviceBootReceiver()
        context.registerReceiver(bootCompletedBroadcastReceiver, intentFiltersForBootCompleted)
        return this
    }

    fun withNtpHost(host: String): AccurateTime {
        this.ntpHost = host
        return this
    }

    fun withCache(isNeedCache: Boolean): AccurateTime {
        this.isNeedCache = isNeedCache
        return this
    }


    fun syncTime() {

        /**
         * Если есть интернет то верну NtpObservable, если во время запроса на сервер происходит ошибка то пробую получить время с геолокации
         *
         */

        disposable = getUtcTimeObservable()
                .subscribeOn(Schedulers.io())
                .onErrorResumeNext { throwable ->
                    run {
                        log(TAG, "syncTime error ${throwable.message}")
                        refContext.get()?.let {
                            if (throwable is NtpException) {
                                return@onErrorResumeNext getGeoObservable(it)
                            } else {
                                throw Exception("Не смог получить время из  ntp и gps")
                            }
                        }
                    }
                }
                .subscribeWith(object : DisposableSingleObserver<Time>() {
                    override fun onSuccess(t: Time) {
                        log(TAG, "onSuccess ${t.formatUtcTime()}")
                        refContext.get()?.let {
                            if (isNeedCache) {
                                cacheObservable = DiskCacheTime(it)
                                cacheObservable?.saveDeltaTime(t)
                            }
                            variableCacheTime = VariableCacheTime(t.getDelta(), t.from)
                        }

                        if (!isDisposed)
                            dispose()
                        isSyncCompleteSubject.onNext(true)
                    }

                    override fun onError(e: Throwable) {
                        log(TAG, "onError ${e.message}")
                        syncTimeWithError = true
                        e.printStackTrace()

                        if (!isDisposed)
                            dispose()
                        isSyncCompleteSubject.onNext(false)
                    }
                })
    }

//    fun syncTime(): Flowable<Time>? {
//
//        /**
//         * Если есть интернет то верну NtpObservable, если во время запроса на сервер происходит ошибка то пробую получить время с геолокации
//         *
//         */
//
//        timeObservable = getUtcTimeObservable()
//                .toFlowable()
//                .share()
//
//        timeObservable?.let {
//
//            disposable = it
//                    .subscribeOn(Schedulers.io())
//                    .onErrorResumeNext(
//                            Function<Throwable, Publisher<Time>> { throwable ->
//                                run {
//                                    log(TAG, "syncTime error ${throwable.message}")
//                                    refContext.get()?.let {
//                                        if (throwable is NtpException) {
//                                            return@Function getGeoObservable(it).toFlowable()
//                                        } else {
//                                            throw Exception("Не смог получить время из  ntp и gps")
//                                        }
//                                    }
//                                }
//                            }
//                    )
//                    .subscribeWith(object : DisposableSubscriber<Time>() {
//                        override fun onComplete() {
//
//                        }
//
//                        override fun onNext(t: Time) {
//                            log(TAG, "onSuccess ${t.formatUtcTime()}")
//                            refContext.get()?.let {
//                                if (isNeedCache) {
//                                    cacheObservable = DiskCacheTime(it)
//                                    cacheObservable?.saveDeltaTime(t)
//                                }
//                                variableCacheTime = VariableCacheTime(t.getDelta(), t.from)
//                            }
//                            dispose()
//                        }
//
//                        override fun onError(e: Throwable) {
//                            log(TAG, "onError ${e.message}")
//                            syncTimeWithError = true
//                            e.printStackTrace()
//                            dispose()
//                        }
//
//                    })
//        }
//        return timeObservable
//    }


    fun reSyncTime() {
        disposable?.let {
            if (!it.isDisposed) it.dispose()
        }
        syncTime()
    }

    fun now(): Time {
        variableCacheTime?.let {
            log(TAG, "now() беру время из переменной")
            return it.getUtcTime()
        }
        if (cacheObservable == null) {
            refContext.get()?.let {
                cacheObservable = DiskCacheTime(it)
            }
        }
        cacheObservable?.let {
            it.getTime()?.let {
                log(TAG, "now() беру время из preferences = ${it.formatUtcTime()}")
                return it
            }
        }
        return Time(System.currentTimeMillis(), TimeFrom.SYSTEM)
    }


    private fun getUtcTimeObservable(): Single<Time> {
        refContext.get()?.let { context ->
            run {
                return if (isNetworkConnected()) {
                    getNtpObservable()
                } else {
                    getGeoObservable(context).subscribeOn(AndroidSchedulers.mainThread())
                }
            }
        }
        return Single.create({ emitter -> emitter.onError(NullContextException()) })
    }

    private fun getNtpObservable(): Single<Time> {
        ntpObservable = NtpServerTime(ntpHost)
        return ntpObservable.getUtcTime()

    }

    private fun getGeoObservable(context: Context): Single<Time> {
        geoObservable = GeoLocationTime(context)
        return geoObservable.getUtcTime()
    }

    private fun isNetworkConnected(): Boolean {
        refContext.get()?.let {
            val connectivityManager = it.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo = connectivityManager.activeNetworkInfo
            return networkInfo != null && networkInfo.isConnected
        }
        return false
    }
}