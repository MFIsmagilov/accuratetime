package com.gradoservice.accuratetime.time.timeproviders

import com.gradoservice.accuratetime.time.model.Time
import io.reactivex.Single

interface TimeProvider {

    fun getUtcTime(): Single<Time>
}